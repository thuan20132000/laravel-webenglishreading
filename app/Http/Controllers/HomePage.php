<?php

namespace App\Http\Controllers;

use App\Model\Comment;
use App\Model\Post;
use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class HomePage extends Controller
{
    //

    public function __construct(){

    }

    public function index(){

        $postTops = Post::orderBy('view','desc')->take(3)->get();
        $postTopsArr = array();
        foreach($postTops as $post){
            array_push($postTopsArr,$post->id);
        }
        $postList = Post::whereNotIn('id',$postTopsArr)->orderBy('id','desc')->paginate(20);
        return view('pages.home',['postTops'=>$postTops,'postList'=>$postList]);
    }

    public function show(Post $post){
        Post::find($post->id)->increment('view');
        $postRecent = Post::orderBy('created_at','desc')->take(6)->inRandomOrder()->get();
        $postSimilar = Post::where('topic_id',$post->topic->id)->inRandomOrder()->take(4)->get();
        $comments = Comment::where('post_id',$post->id)->orderBy('created_at','desc')->paginate(10);

        $postCurrent = $post;

        return view('pages.post-single',['postCurrent'=>$postCurrent,'post'=>$post,'postSimilar'=>$postSimilar,'postRecent'=>$postRecent,'comments'=>$comments]);
    }

    public function topicPage(Topic $topic){
        $postsByTopic = Post::where('topic_id',$topic->id)->inRandomOrder()->paginate(20);
        return view('pages.topic-page',['topic'=>$topic,'postsByTopic'=>$postsByTopic]);
    }

    public function postComments(Post $post,Request $request){

        $errors = null;
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:50',
            'email'=>'required|email',
            'body'=>'required|max:255'
        ],[
            'name.max'=>"You need to name with 50 characters!",
            'email.email'=>"You need to enter your email exactly!",
            'body.max'=>"you need to enter comment with not more than 150 characters!"
        ]);

        if ($validator->fails()) {
            return response()->json(['state'=>$validator]);
        }

        $validated = $request->validate([
            'name'=>'required|max:50',
            'email'=>'required|email',
            'body'=>'required|max:255'
        ],[
            'name.max'=>"You need to a short name!",
            'email.email'=>"You need to enter your email exactly!",
            'body.max'=>"you need to a short comment"
        ]);

        $comment = new Comment();
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->body = $request->body;
        $comment->post_id = $post->id;
        $comment->save();

        return response()->json([
            'state'=>$errors
        ]);

    }
}
