<?php

namespace App\Http\Controllers;

use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;



class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $topics = Topic::all();
        return view('admin.topic.index',['topics'=>$topics]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated =$request->validate([
            'name'=>'required',
        ]);
        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3-ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $topic = new Topic();
        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name);
        $topic->status = $request->status == "on"?1:0;
        $topic->image = $imageUrl;
        $topic->save();
        return redirect()->back()->with("success", "Successfull");


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        //
        return view('admin.topic.edit',['topic'=>$topic]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        //
        $validated =$request->validate([
            'name'=>'required',
        ]);

        $imageUrl = null;
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3-ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name);
        $topic->status = $request->status == "on"?1:0;
        if($imageUrl){
            $topic->image = $imageUrl;
        }
        $topic->save();
        return redirect()->back()->with("success", "Successfull");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        //
        Topic::find($topic->id)->delete();
        return redirect()->back()->with("deleted successfull");
    }



    // Delete checked topics ajax
    public function deleteAll(Request $request){

        $ids = $request->ids;
        $errors = null;
        try {
            //code...
            Topic::whereIn('id',$ids)->delete();
        } catch (\Throwable $th) {
            //throw $th;
            throw $errors  = $th;
        }

        return response()->json([
            'data'=>$errors
        ]);
    }
}
