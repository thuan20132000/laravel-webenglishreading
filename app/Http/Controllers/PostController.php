<?php

namespace App\Http\Controllers;

use App\Model\Post;
use App\Model\Tag;
use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::all();
        return view('admin.post.index',['posts'=>$posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $topics =Topic::all();
        $tags = Tag::all();
        return view('admin.post.create',['topics'=>$topics,'tags'=>$tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validated = $request->validate([
            'topic'=>'required',
            'title'=>'required|max:255',
            'body'=>'required',
        ]);

        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            // $img->resize(null, 600, function ($constraint) {
            //     $constraint->aspectRatio();
            // });

            $img->text('elreading.club', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3.ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $post = new Post;
        $post->topic_id = $request->topic;
        $post->title = $request->title;
        $post->slug = Str::slug($request->title);
        $post->body = $request->body;
        $post->status = $request->status == "on"?1:0;
        $post->image = $imageUrl;
        $post->save();

        return redirect()->back()->with("success","successfull");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $topics = Topic::all();
        $tags = Tag::all();
        return  view('admin.post.edit',['post'=>$post,'topics'=>$topics,'tags'=>$tags]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $validated = $request->validate([
            'topic'=>'required',
            'title'=>'required|max:255',
            'body'=>'required',
        ]);

        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            // $img->resize(200, 200, function ($constraint) {
            //     $constraint->aspectRatio();
            // });

            $img->text('elreading.club', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3.ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $post->topic_id = $request->topic;
        $post->title = $request->title;
        $post->slug = Str::slug($request->title);
        $post->introduction = $request->introduction;
        $post->body = $request->body;
        $post->status = $request->status == "on"?1:0;
        $post->image = $imageUrl;
        $post->save();

        return redirect()->back()->with("success","successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        Post::find($post->id)->delete();
        return redirect()->back();
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $errors = null;
        try {
            //code...
            Post::whereIn('id',$ids)->delete();
        } catch (\Throwable $th) {
            //throw $th;
            throw $errors  = $th;
        }

        return response()->json([
            'data'=>$errors
        ]);
    }
}
