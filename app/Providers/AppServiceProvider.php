<?php

namespace App\Providers;

use App\Model\Topic;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        $topicNavigation = Topic::all()->take(8);
        $topicMenu = Topic::all();
        view()->share('topicMenu', $topicMenu);
        view()->share('topicNavigation',$topicNavigation);
    }
}
