<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public function path(){
        return route('posts.show',$this);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function topic(){
        return $this->belongsTo(Topic::class);
    }
}
