<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/','HomePage@index')->name('page.home');

Route::get('/posts/{post}/{slug}','HomePage@show')->name('post.show');
Route::post('/post/{post}','HomePage@postComments')->name('post.comment');
Route::get('/topic/{topic}/{slug}','HomePage@topicPage')->name('topic.page');


Route::group(['prefix' => 'manager'], function () {
    Route::get('/','AdminHomeController@index');

    Route::resource('/topic', 'TopicController');
    Route::delete('/topic','TopicController@deleteAll');

    Route::resource('/posts', 'PostController');
    Route::delete('/posts','PostController@deleteAll');

});
