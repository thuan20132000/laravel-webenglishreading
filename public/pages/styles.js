



$('document').ready(function(){


    $('.navbar-menu-icon').on('click',function(){
        $('.navbar-nav').toggle();
    })


    $('#btn-comment').on('click',function(){
        $('.form-error-message').remove();
        $('.form-success-message').remove();

    var name = $('.form-comment input[name=name]').val();
    var email = $('.form-comment input[name=email]').val();
    var body = $('.form-comment textarea[name=body]').val();
    var post_id = $('.form-comment input[name=post_id]').val();

        console.log(post_id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '/post/'+post_id,
        data: {
            name: name,
            email:email,
            body:body,
        },
        dataType: 'json',
        success: function(data) {
            console.log(data.state);
            if(data.state){
                $('.form-comment').before('<small class="form-error-message"> Please check your email,name and comment!!</small>');
            }
            if(!data.state){
                $('.form-comment').before('<small class="form-success-message">Comment Successful.</small>');

                setTimeout(() => {
                    location.reload();
                }, 1200);
            }
        }
    });

    })


});
