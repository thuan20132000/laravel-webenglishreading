

     $(".js-select2-dropdown").select2({
        tags: true
    });


    $(document).ready(function(){
        $('#dataTable').DataTable();
    });

    // Preview Image
    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadForm + img').remove();
                $('#uploadForm').after('<img src="' + e.target.result + '" width="220" height="220"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#file").change(function() {
        filePreview(this);
    });


    //select check box
    function check_item(name){
        return $('#'+name+'-checkAll').on('click',function(){
            $('input[name="'+name+'"]').prop('checked', this.checked);
        });

    }
    //Add name to check here
    check_item("topic");
    check_item("posts");



    //delete checked
    function delete_item(name){

        return $('#'+name+'-deleteChecked').on('click',function(){

            var dataArray = [];
            $.each($("input[name='"+name+"']:checked"), function() {
                dataArray.push($(this).val());
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url: name,
                data: {
                    'ids': dataArray
                },
                success: function(res) {
                    if(!res.data){
                        toastr.success('Deleted successfull');
                        setTimeout(() => {
                            location.reload();
                        }, 1200);
                    }else{
                        toastr.error('Deleted failed!!');
                    }
                }
            });

        });
    }
    //Add name to delete here
    delete_item('topic');
    delete_item('posts');



