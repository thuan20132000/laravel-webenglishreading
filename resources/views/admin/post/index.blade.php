@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">posts</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            {{--  --}}
            <div id="table-setting">
                <div class="row">
                    <div class="col-6">
                        <div style="display: flex;flex-direction:row;justify-content: flex-start;align-items: center">
                            <input type="checkbox" name="" id="posts-checkAll" style="margin-left:3%">
                            <input type="button" class="btn btn-danger" id="posts-deleteChecked" style="margin:10px" value="Delete All" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div style="display: flex;flex-direction:row;justify-content: flex-end">
                            <a href="{{route('posts.create')}}" class="btn btn-success" style="margin:10px">Add new</a>
                        </div>
                    </div>
                </div>
            </div>
            {{--  --}}
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap" id="dataTable">
                <thead>
                  <tr>
                    <th>
                    </th>
                    <th>STT</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Setting</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($posts  as $key => $post)
                    <tr style="cursor: pointer">
                        <th>
                            <input value="{{$post->id}}" type="checkbox" name="posts" id="post-{{$post->id}}">
                        </th>
                        <td>{{$key}}</td>
                        <td>{{$post->title}}</td>
                        <td>
                            <img width="120px" height="100px" src="{{$post->image?$post->image:""}}" alt="">
                        </td>
                        <td><span class="tag tag-success">{{$post->status}}</span></td>
                        <td style="display: flex;justify-content: space-between">
                            <a href="{{route('posts.edit',$post->id)}}"><i class="fas fa-edit" style="font-size:24px;color:blue"></i></a>
                            <a href="#" onclick="document.getElementById('formDelete-{{$post->id}}').submit()"><i class="fas fa-trash-alt" style="font-size: 24px;color:red"></i></a>
                            <form action="{{route('posts.destroy',$post->id)}}" method="POST" id="formDelete-{{$post->id}}" style="display:none">
                                @method('DELETE')
                                @csrf
                                <button type="submit">Delete</button>
                            </form>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          {{-- <div style="width:300px;margin:auto">
              {{ $posts->links() }}
          </div> --}}
        </div>
      </div>
</div>

@endsection
