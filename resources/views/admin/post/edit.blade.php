
@extends('admin.layouts.master')

@section('content')
<style>
    .select2-container .select2-selection--single{
        height: auto;
    }
</style>
<div class="content-wrapper">
    <form role="form" method="POST" action="{{ route('posts.update',$post->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row" style="margin:10px">
            <div class="col-6">
                @if(session()->has('success'))
                <div class="alert alert-success" style="width:200ox;margin:auto">
                    {{ session()->get('success') }}
                </div>
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Topic</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <div class="card-body">
                        <div class="form-group">
                            <label>Topic</label>
                            <select name="topic" class="form-control select2 select2-hidden-accessible js-select2-dropdown  @error('topic') is-invalid @enderror " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <option value="">-----Selecting Topic-----</option>
                                @foreach ($topics as $topic)
                                    <option value="{{$topic->id}}" {{$post->topic->id ===$topic->id?"selected":""}} >{{$topic->name}}</option>
                                @endforeach
                            </select>
                            @error('topic')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTitle">Title</label>
                            <input name="title" value="{{$post->title}}" type="name" class="form-control  @error('title') is-invalid @enderror " id="inputTitle" placeholder="Enter Title">
                            @error('title')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputIntroduction">Introduction</label>
                            <textarea name="introduction" class="form-control  @error('introduction') is-invalid @enderror " id="inputIntroduction" rows="3" placeholder="Enter Introduction">
                                {{$post->introduction}}
                            </textarea>
                            @error('introduction')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>


                        <div class="form-check">
                        <input name="status" type="checkbox" {{$post->status ===1?"checked":""}} class="form-check-input" id="inputStatus">
                        <label class="form-check-label" for="inputStatus">Status</label>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Topic</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <div class="card-body">
                        <div class="form-group">
                            <label>Tag</label>
                            <select name="tags[]" multiple="multiple" class="form-control select2 select2-hidden-accessible js-select2-dropdown  @error('tag') is-invalid @enderror " style="width: 100%;color:red" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                @foreach ($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>

                            @error('tag')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Image</label>
                            <div class="custom-file" id="uploadForm">
                                <input name="image" id="file" type="file" class="custom-file-input  @error('image') is-invalid @enderror " id="inputImage">
                                <label class="custom-file-label" for="inputImage">Choose Image</label>
                                <img src="{{$post->image}}" alt="">
                                @error('image')
                                <span class="error invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                </div>

            </div>
            </div>
            <div class="col-12">
                <div class="card-body pad">
                    <textarea name="body" id="editor1" rows="10" cols="80" class="@error('body') is-invalid @enderror">
                    {{$post->body}}
                    </textarea>
                    @error('body')
                    <span class="error invalid-feedback">{{$message}}</span>
                    @enderror
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace( 'editor1' );
                    </script>
                </div>
            </div>

        </div>
    </form>

</div>

@endsection
