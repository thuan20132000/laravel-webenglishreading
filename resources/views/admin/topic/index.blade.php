@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Topics</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            {{--  --}}
            <div id="table-setting">
                <div class="row">
                    <div class="col-6">
                        <div style="display: flex;flex-direction:row;justify-content: flex-start;align-items: center">
                            <input type="checkbox" name="" id="topic-checkAll" style="margin-left:3%">
                            <input type="button" class="btn btn-danger" id="topic-deleteChecked" style="margin:10px" value="Delete All" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div style="display: flex;flex-direction:row;justify-content: flex-end">
                            <a href="{{route('topic.create')}}" class="btn btn-success" style="margin:10px">Add new</a>
                        </div>
                    </div>
                </div>
            </div>
            {{--  --}}
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap" id="dataTable">
                <thead>
                  <tr>
                    <th>
                    </th>
                    <th>STT</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Setting</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($topics  as $key => $topic)
                    <tr>
                        <th>
                            <input value="{{$topic->id}}" type="checkbox" name="topic" id="topic-{{$topic->id}}">
                        </th>
                        <td>{{$key}}</td>
                        <td>{{$topic->name}}</td>
                        <td>{{$topic->slug}}</td>
                        <td>
                            <img width="120px" height="100px" src="{{$topic->image?$topic->image:""}}" alt="">
                        </td>
                        <td><span class="tag tag-success">{{$topic->status}}</span></td>
                        <td style="display: flex;justify-content: space-between">
                            <a href="{{route('topic.edit',$topic->id)}}"><i class="fas fa-edit" style="font-size:24px;color:blue"></i></a>
                            <a href="#" onclick="document.getElementById('formDelete-{{$topic->id}}').submit()"><i class="fas fa-trash-alt" style="font-size: 24px;color:red"></i></a>
                            <form action="{{route('topic.destroy',$topic->id)}}" method="POST" id="formDelete-{{$topic->id}}" style="display:none">
                                @method('DELETE')
                                @csrf
                                <button type="submit">Delete</button>
                            </form>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          {{-- <div style="width:300px;margin:auto">
              {{ $topics->links() }}
          </div> --}}
        </div>
      </div>
</div>

@endsection
