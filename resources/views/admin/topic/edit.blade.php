
@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">

<div class="row" style="margin:10px">
    <div class="col-6">
        @if(session()->has('success'))
        <div class="alert alert-success" style="width:200ox;margin:auto">
            {{ session()->get('success') }}
        </div>
        @endif
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Topic Edit</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('topic.update',$topic)}}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="inputName">Name</label>
                  <input value="{{$topic->name}}" name="name" type="name" class="form-control" id="inputName" placeholder="Enter name">
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Topic Image</label>
                  <div class="input-group">
                    <div class="custom-file" id="uploadForm">
                      <input name="image" id="file" type="file" class="custom-file-input" id="inputImage">
                      <label class="custom-file-label" for="inputImage">Choose Image</label>
                    </div>
                    <img name="image_prev" src="{{url($topic->image?$topic->image:"")}}" alt="" sizes="" srcset="">
                    <div class="input-group-append">
                      <span class="input-group-text" id="">Upload</span>
                    </div>
                  </div>
                </div>
                <div class="form-check">
                  <input checked type="checkbox" class="form-check-input" id="inputStatus">
                  <label class="form-check-label" for="inputStatus">Status</label>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
    </div>
</div>

</div>
@endsection
