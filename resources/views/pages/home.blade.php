
@extends('pages.layouts.master')

@section('content')

<section class="section first-section">
    <div class="container-fluid">
        <div class="masonry-blog clearfix">

            <div class="left-side">
                <div class="masonry-box post-media">
                     <img src="{{$postTops[0]->image}}" alt="" class="img-fluid" style="height:468px;width:100%">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-aqua"><a href="{{route('topic.page',[$postTops[0]->topic,$postTops[0]->topic->name])}}" title="">{{$postTops[0]->topic->name}}</a></span>
                                <h4 style="background-color:#2eb174b5;border-radius:6px;padding-left:12px"><a href="{{route('post.show',[$postTops[0],$postTops[0]->slug])}}" title="">{{$postTops[0]->title}}</a></h4>
                                <small class="view-count"><a href="#" title=""><i class="fa fa-eye"></i>{{$postTops[0]->view}}</a></small>
                            </div><!-- end meta -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->
            </div><!-- end left-side -->

            <div class="center-side">
                <div class="masonry-box post-media">
                     <img src="{{$postTops[1]->image}}" alt="" class="img-fluid" style="height:468px;width:100%">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-aqua"><a href="{{route('topic.page',[$postTops[1]->topic,$postTops[1]->topic->name])}}" title="">{{$postTops[1]->topic->name}}</a></span>
                                <h4 style="background-color:#2eb174b5;border-radius:6px;padding-left:12px"><a href="{{route('post.show',[$postTops[1],$postTops[1]->slug])}}" title="">{{$postTops[1]->title}}</a></h4>
                                <small class="view-count"><a href="#" title=""><i class="fa fa-eye"></i>{{$postTops[1]->view}}</a></small>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                </div><!-- end post-media -->
            </div><!-- end left-side -->

            <div class="right-side hidden-md-down">
                <div class="masonry-box post-media">
                     <img src="{{$postTops[2]->image}}" alt="" class="img-fluid" style="height:468px;width:100%">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-aqua"><a href="{{route('topic.page',[$postTops[2]->topic,$postTops[2]->topic->name])}}" title="">{{$postTops[2]->topic->name}}</a></span>
                                <h4 style="background-color:#2eb174b5;border-radius:6px;padding-left:12px"><a href="{{route('post.show',[$postTops[2],$postTops[2]->slug])}}" title="">{{$postTops[2]->title}}</a></h4>
                                <small class="view-count"><a href="#" title=""><i class="fa fa-eye"></i>{{$postTops[2]->view}}</a></small>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                </div><!-- end post-media -->
            </div><!-- end right-side -->
        </div><!-- end masonry -->
    </div>
</section>

<section class="section wb">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">

                <div class="page-wrapper">
                    <div class="blog-list clearfix">

                        @foreach ($postList as $post)
                            <div class="blog-box row">
                                <div class="col-md-4">
                                    <div class="post-media">
                                        <a href="{{route('post.show',[$post,$post->slug])}}" title="">
                                            <img src="{{$post->image}}" alt="" class="img-fluid" style="width:250px;height:180px;object-fit:cover">
                                            <div class="hovereffect"></div>
                                        </a>
                                    </div><!-- end media -->
                                </div><!-- end col -->

                                <div class="blog-meta big-meta col-md-8">
                                    <span class="bg-aqua"><a href="{{route('topic.page',[$post->topic,$post->topic->name])}}" title="">{{$post->topic->name}}</a></span>
                                    <h4><a href="{{route('post.show',[$post,$post->slug])}}" title="">{{$post->title}}</a></h4>
                                    <p>{!!Str::limit($post->body,250)!!}</p>
                                    <small class="view-count"><a href="#" title=""><i class="fa fa-eye"></i>{{$post->view}}</a></small>
                                    {{-- <small><a href="{{ asset('forest-time/garden-single.html') }}" title="">{{$post->created_at}}</a></small> --}}
                                </div><!-- end meta -->
                            </div><!-- end blog-box -->
                            <hr class="invis">
                        @endforeach

                    </div><!-- end blog-list -->
                </div><!-- end page-wrapper -->

                <hr class="invis">

                <div class="pagination-wrapper">
                        {{ $postList->onEachSide(5)->links() }}
                </div><!-- end row -->
            </div><!-- end col -->

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="sidebar">
                    {{-- <div class="widget">
                        <h2 class="widget-title">Search</h2>
                        <form class="form-inline search-form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search on the site">
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </form>
                    </div><!-- end widget --> --}}

                    <div class="widget">
                        {{-- <h2 class="widget-title">Topics</h2> --}}
                        <div class="blog-list-widget">
                            <div class="list-group">

                                @foreach ($topicMenu as $topic)
                                    <a href="{{route('topic.page',[$topic,$topic->name])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="w-100 justify-content-between topicMenuRight">
                                            <h5 class="mb-1" >{{$topic->name}}</h5>
                                        </div>
                                    </a>
                                @endforeach


                            </div>
                        </div><!-- end blog-list -->
                    </div><!-- end widget -->



                </div><!-- end sidebar -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>


@endsection
