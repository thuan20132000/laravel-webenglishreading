

@extends('pages.layouts.master')

@section('content')

<div class="page-title wb">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <h2><i class="fa fa-bookmark bg-green"></i> Topic by: {{$topic->name}}</h2>
            </div><!-- end col -->
            <div class="col-lg-4 col-md-4 col-sm-12 hidden-xs-down hidden-sm-down">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('page.home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Topic</a></li>
                </ol>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end page-title -->


<section class="section wb">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div class="page-wrapper">
                    <div class="blog-list clearfix">

                        @foreach ($postsByTopic as $post)
                            <div class="blog-box row">
                                <div class="col-md-4">
                                    <div class="post-media">
                                        <a href="{{ route('post.show', [$post,$post->slug]) }}" title="">
                                            <img src="{{$post->image}}" alt="" class="img-fluid" style="width:100%;height:180px;object-fit:cover">
                                            <div class="hovereffect"></div>
                                        </a>
                                    </div><!-- end media -->
                                </div><!-- end col -->

                                <div class="blog-meta big-meta col-md-8">
                                    <span class="bg-aqua"><a href="{{route('topic.page',[$topic,$topic->name])}}" title="">{{$post->topic->name}}</a></span>
                                    <h4><a href="{{route('post.show', [$post,$post->slug])}}" title="">{{$post->title}}</a></h4>
                                    <p>{!!Str::limit($post->body,150)!!}</p>
                                    <small class="view-count"><a href="#" title=""><i class="fa fa-eye"></i>{{$post->view}}</a></small>
                                </div><!-- end meta -->
                            </div><!-- end blog-box -->
                        @endforeach



                    </div><!-- end blog-list -->
                </div><!-- end page-wrapper -->

                <hr class="invis">
                <div class="row">
                    <div class="col-md-12">
                        {{ $postsByTopic->links() }}
                    </div>
                </div>
            </div><!-- end col -->

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="sidebar">


                    <div class="widget">
                        <div class="blog-list-widget">
                            <div class="list-group">

                                @foreach ($topicMenu as $topic)
                                    <a href="{{ route('topic.page', [$topic,$topic->name]) }}" class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="w-100 justify-content-between topicMenuRight">
                                            <img src="upload/garden_sq_09.jpg" alt="" class="img-fluid float-left">
                                            <h5 class="mb-1">{{$topic->name}}</h5>
                                        </div>
                                    </a>
                                @endforeach


                            </div>
                        </div><!-- end blog-list -->
                    </div><!-- end widget -->


                </div><!-- end sidebar -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>
@endsection
