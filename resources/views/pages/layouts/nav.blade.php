

<div id="wrapper">


    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo logo-header">
                        <a href="{{ route('page.home')}}"><img src="{{ asset('logo-elreading.png') }}" alt=""></a>
                    </div><!-- end logo -->
                </div>
            </div><!-- end row -->
        </div><!-- end header-logo -->
    </div><!-- end header -->

    <header class="header">
        <div class="container">
            <nav class="navbar navbar-inverse navbar-toggleable-md">
                <button class="navbar-toggler navbar-menu-icon" type="button" >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-menu-content">
                    <ul class="navbar-nav">

                        @foreach ($topicNavigation as $topic)
                            <li class="nav-item">
                                <a class="nav-link " href="{{ route('topic.page', [$topic,$topic->name]) }}">{{$topic->name}}</a>
                            </li>
                        @endforeach


                    </ul>
                </div>
            </nav>
        </div><!-- end container -->
    </header><!-- end header -->
