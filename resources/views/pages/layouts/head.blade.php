
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{--  <!-- You can use open graph tags to customize link previews.  --}}
      @if (Route::currentRouteName() == 'post.show')
        <meta property="og:url"           content="{{route('post.show',[$postCurrent,$postCurrent->slug])}}" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="{{$postCurrent->title}}" />
        <meta property="og:description"   content="{!!Str::limit($postCurrent->body,250)!!}" />
        <meta property="og:image"         content="{{$postCurrent->image}}" />
      @endif


    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Site Metas -->
    <title>El Reading</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Site Icons -->

    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('faviconcustom.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('faviconcustom.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('faviconcustom.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('faviconcustom.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('faviconcustom.ico/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('faviconcustom.ico//ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- Design fonts -->
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('forest-time/css/bootstrap.css') }}" rel="stylesheet">

    <!-- FontAwesome Icons core CSS -->
    <link href="{{ asset('forest-time/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('forest-time/style.css') }}" rel="stylesheet">

    <!-- Responsive styles for this template -->
    <link href="{{ asset('forest-time/css/responsive.css') }}" rel="stylesheet">

    <!-- Colors for this template -->
    <link href="{{ asset('forest-time/css/colors.css') }}" rel="stylesheet">

    <!-- Version Garden CSS for this template -->
    <link href="{{ asset('forest-time/css/version/garden.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- custom CSS --}}
    <link rel="stylesheet" href="{{ asset('pages/styles.css') }}">

</head>
<body>
