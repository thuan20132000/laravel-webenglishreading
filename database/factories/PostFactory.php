<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Post;
use App\Model\Topic;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        'title'=> $faker->word,
        'slug'=>$faker->slug,
        'introduction'=>$faker->paragraph,
        'body'=>$faker->paragraph,
        'image'=>'default.jpg',
        'topic_id'=>function(){
            return Topic::all()->random();
        }
    ];
});
