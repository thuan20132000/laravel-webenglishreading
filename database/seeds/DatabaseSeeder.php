<?php

use App\Model\Post;
use App\Model\Topic;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        factory(Topic::class,10)->create();
        factory(Post::class,20)->create();
    }
}
